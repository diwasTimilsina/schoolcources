/**
 * 334
 * Tony Liu and Diwas Timilsina
 * Eat a lotta calories 
 */


object Dennys {

  def main (args : Array[String]) = {
    val lines = scala.io.Source.fromFile("dennys.txt").getLines();
    val data = lines.toList;
    
    //Part a
    for (i <- 0 to data.length-1) println(data(i));

    //Part b
    data.foreach(println(_));

    //Part c
    for (i <- 0 to data.length - 1 if (data(i).toLowerCase.contains("slam"))) println(data(i));

    //Part d
    for (i <- 0 to data.length - 1 if (data(i).toLowerCase.contains("slam")) 
				   if !data(i).toLowerCase.contains("junior")
				   if !data(i).toLowerCase.contains("senior")) 
	println(data(i));


    //Part e
    val list = for(d <- data; if d.toLowerCase.contains("slam")) yield d.toUpperCase;
    for (i <- 0 to list.length - 1) println(list(i));    

    //Part f
    val items = data.map( (x : String) => new Item(x));
    for (i <- 0 to items.length - 1) println(items(i));

    //Part g
    val unhealthy = for ( x <- items if x.calories > 1000) yield x;
    for (i <- 0 to unhealthy.length - 1) println(unhealthy(i));

    //Part h 
    val sorted = items.sortWith(_.calories < _.calories)
    for (i <- 0 to sorted.length - 1) println(sorted(i));

    println(sorted.foldLeft (0) ((result,x) => result + x.calories));
  }

//Part f
class Item(s: String) {
  def name = s.substring(s.indexOf(":") + 1);
  def calories = s.substring(0 , s.indexOf(":")).toInt;
  
  override def toString() : String = {
    return (this.name + " Calories: " + "%5d ".format(this.calories)) ;
  }
}

}
