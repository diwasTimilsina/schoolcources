;; Hw #1 
;;
;; (c) 2014 Diwas Timilsina, Rebecca Lewis
;; 

'--------------------------------------------
;;---------------------------------------------
;;a) Recursive Defination
;;---------------------------------------------

;; function to compute square
(defun square (x) (* x x))

;; function to compute fast exponent 
(defun fastexp (base exp)
  (cond ((eq exp 1)           base)
	((eq (mod exp 2) 0)   (square(fastexp base (/ exp 2))))
 	(t                    (* base (fastexp base (- exp 1))))))

;; examples for the fast exponent
(fastexp '2 '1)
(fastexp '2 '2)
(fastexp '2 '3)

'------------------------------------------------


;; proof is attached at the end of the problem set

;;--------------------------------------------
;;b) Recursive list manipulation 
;;--------------------------------------------

;; function to merge two list
(defun merge-list (listA  listB)
  (cond ((eq  listA nil)  listB)
	((eq  listB nil)  listA)
	(t               (append (list (car listA) (car listB)) (merge-list (cdr listA) (cdr listB))))))


;; examples
(merge-list '(A B C) nil)
(merge-list nil '(A B C))
(merge-list '(A B C) '(1 2 4))
(merge-list '((1 2)(3 4)) '(A B)) 

'------------------------------------------------


;;-----------------------------------------------
;;c) Reverse 
;;-----------------------------------------------

;; reverse the list
(defun rev (l)
  (cond ((atom l)   l)
      	(t        (append (rev (cdr l)) (list (car l))))))


;; examples
(rev nil)
(rev 'A )
(rev '(A B C D))
(rev '(A (B C) D))
(rev '((A B) (C D)))

'------------------------------------------------
 
;;------------------------------------------------
;;d) mapping functions
;;------------------------------------------------

;; function to check if the word is censored or not
;; if the word is censored, then return XXXX otherwise
;; just return the word
(defun censor-word (word)
  (cond ((member word '(extension algorithms graphics AI midterm)) 'XXXXX)
	(t                                                          word)))

;; function to produce the censored list
(defun censor (list)
  (cond ((atom list) (censor-word list) )
	(t           (cons (censor-word (car list)) (censor (cdr list))))))

;; examples
(censor nil)
(censor '(Apple Ball Cow Moo AI graphics)) 
(censor '(I LIKE PROGRAMMING LANGUAGES MORE THAN GRAPHICS AND ALGORITHMS))

'------------------------------------------------

;;-----------------------------------------------
;; e) Working with Structured Data
;;-----------------------------------------------

;; Define a variable holding the data
(defvar grades '((Riley (90.0 33.3))
		 (Jessie (100.0 85.0 97.0))
		 (Quinn  (70.0 100.0))))


;; function to lookup the grades of the students
(defun lookup (student l)
  (cond ((eq (length l) 0)               nil) 
	((eq student nil)                nil)
	((eq student (car(car l)))      (car(cdr(car l))))
	(t                              (lookup student (cdr l)))))  

; lookup example
(lookup 'Diwas grades) 
(lookup 'Riley grades)
(lookup 'Quinn grades) 
(lookup 'Jessie grades)
(lookup 'Rebecca grades)

;; average of a student
;; this function calls calcumate sum function to 
;; compute the total sum of the grades before calculating
;; the average 
(defun student-ave (l)
  (cond ((eq l nil) nil)
	(t          (list (car l) (/ (calculate-sum(lookup (car l) grades)) (length  (lookup (car l) grades))))))) 

 
;; function to calculate the sum of grades of a student
(defun calculate-sum (l)
  (cond ((eq (length l) 0) 0.0)
	(t                ( + (car l) (calculate-sum(cdr l))))))

;; compute the average of all students
(defun averages (l)
  (cond ((eq (length l) 0) nil)
	(t                (cons (student-ave (car l)) (averages (cdr l))))))

;; check for average function 
(averages grades)


;; function to compare two averages
(defun compare-students (list1 list2)
  (cond ((< (car(cdr list1)) (car(cdr list2))) t)
	(t                                      nil)))

;; test for compare students
(compare-students '(RILEY 61.25) '(JESSIE 94.0))
(compare-students '(JESSIE 94.0) '(RILEY 61.25)) 
  

;; test for the sorting students
(sort(averages grades) #'compare-students)

'------------------------------------------------


;;------------------------------------------------
;; f) Deep Reverse
;;------------------------------------------------

;;deep reverse function uses rev function 
(defun  deep-rev (l)
  (cond ((atom l) l)
	(t        (mapcar #'deep-rev (rev l)))))
 

;; example for deep reverse
(deep-rev '(A (B C) D))
(deep-rev 'A)
(deep-rev nil)
(deep-rev '(1 2 ((3 4) 5)))
(deep-rev '(1 2 ((3 4) 5 ( 6 (7 8) ))))

'------------------------------------------------

;;--------------------------------------------------
;; g) Optional Lips for the Parenthetically Inclined
;;--------------------------------------------------




;; locate value e in list1 
(defun locate (l1 e)
  
  ;;if value e is null
  (if (eq e nil)         
      l1) 
  
  ;; done if we found the value e in root  
  ;; just return the list
  (if (eq e (car l1))     
      l1)
  
  ;; if e is less than left the node, move to left subtree
  ;; otherwise move towards the right subtree
  (if ( < (car l1) e)  
      (defvar child (car(cdr tree)))
    (defvar child (car(cdr(cdr tree)))))
  
  ;; if the subtree doesn't have any children, just return the node
  ;; otherwise call locate on the subtree
  (if (eq child nil)    
      l1
    (locate child e)))

	 
 
;; predefined tree variable
(defvar tree '(10 (5 (3 nil nil) nil) (13 (7 nil nil) (16 nil nil))))



