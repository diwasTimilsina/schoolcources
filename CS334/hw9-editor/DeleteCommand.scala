

/**
 * InsertCommand class
 * 
 * Diwas Timilsina
 * hw 9
 * 
 */

class DeleteCommand (b:Buffer, val count: Int) extends EditCommand(b){

  val text:String = b.getText(b.getCursor(),b.getCursor()+count);
  val index:Int = b.getCursor();

  // this method carries out the desired operation in the target 
  // buffer
  override def execute():Unit={
    b.delete(count);
  }
  
  // this method carries out the inverse of the execute operation
  override def undo():Unit={
    b.insert(text);
    b.setCursor(index);
  }

  // print out what the connand represents
  override def toString():String={
   "[ Delete " + count + " ]";
  }
}  
