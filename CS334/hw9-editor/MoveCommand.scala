
/**
 * InsertCommand class
 * 
 * Diwas Timilsina
 * hw 9
 * 
 */

class MoveCommand (b:Buffer, val loc: Int) extends EditCommand(b){
  
  // this method carries out the desired operation in the target 
  // buffer
  
  // cursor location before move
  val index:Int = b.getCursor();

  // cursor after move
  var afterMove:Int = 0;
  
  override def execute():Unit={
    val l = if (loc < 0) { 
      0 
    } else if (loc > b.size()) { 
       b.size() 
    } else {
      loc
    }

    afterMove = l;
    b.setCursor(l);
 
  }
  
  // this method carries out the inverse of the execute operation
  override def undo():Unit={
    b.setCursor(index);
  }

  // print out what the connand represents
  override def toString():String={
    "[ Move to " + afterMove+" ]";
  }

}  
