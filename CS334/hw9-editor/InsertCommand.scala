

/**
 * InsertCommand class
 * 
 * Diwas Timilsina
 * hw 9
 * 
 */

class InsertCommand (b:Buffer, val text: String) extends EditCommand(b){
  
  // this method carries out the desired operation in the target 
  // buffer
  override def execute():Unit={
    b.insert(text);
    b.setCursor(b.getCursor()+text.length());
  }
  
  // this method carries out the inverse of the execute operation
  override def undo():Unit={
    b.setCursor(b.getCursor()-text.length());
    b.delete(text.length());
  }

  // print out what the connand represents
  override def toString():String={
    "[ Insert "+ text + " ]"; 
  }

}  
