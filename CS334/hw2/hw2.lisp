;; Hw #2 
;; 22 Feb 2014
;;
;; (c) 2014 Diwas Timilsina


;;+++++++++++++++++++++++++++++++++
;; P1 (a) Filter Function
;;+++++++++++++++++++++++++++++++++

;; filter function takes in a predicate function f and 
;; a list l. The function returns a list of those elements in l
;; that satisfied the criteria specified by f

(defun filter (f l)
        
         ;; base case if the list is empty 
  (cond ((eq l nil)                 nil)

        ;; if the first element of l satisfies the criteria of f
        ;; include this element in the list and recursively call 
        ;; filter on rest of the list
	((eq (funcall f (car l)) t) (cons (car l) (filter f (cdr l))))


        ;; otherwise just call filter on the rest of the list
	(t                          (filter f (cdr l)))))


;; examples for filer

;; 1) even list
(defun even (x) (eq (mod x 2) 0))
(filter #'even '(1 3 4 2 6 9 12 4))

;; 2) positive integer
(filter #'(lambda (x) (>= x 0)) '(-1 2 1 -3 -5 4))
 

;;++++++++++++++++++++++++++++++++++++++++++++++++
;;(b) Union and Intersection of Sets
;;++++++++++++++++++++++++++++++++++++++++++++++++


;; function that returns a list of union of two sets 
(defun union-set (l1 l2)

  ;; base cases: if one of the set is empty
  ;; the union is just the other set
  (cond ((eq l1 nil) l2)
	((eq l2 nil) l1)

       ;; the function fed into filter checks if an element of list  
	( t         (append (filter #'(lambda (x) (eq (member x l2) nil)) l1) l2))))


;;function that returns a list of intersection of two sets
(defun intersection-set (l1 l2)
  (cond ((eq l1 nil) nil)
	((eq l2 nil) nil)
	( t         (filter #'(lambda (x) (cond ((eq (member x l2) nil) nil)
						(t                      t))) l1))))


;; examples to test union and intersection of two sets
(union-set '(1 2 3) nil)
(intersection-set nil '(1 2 3))
(union-set '(1 2 3) '(2 3 4 A B C D))
(intersection-set '(1 2 3 4 5 6 7 A) '(A 0 2 3 4 5 6 7 8 9 10))

 	      
;;+++++++++++++++++++++++++++++++++++++++++++++++++
;;(c) Exists Function
;;+++++++++++++++++++++++++++++++++++++++++++++++++

;; exists function to check if there is 
;; atleast one element that satisfies the condition of
;; the predicate function f

(defun exists (f l)
  (cond ((eq (length (filter f l)) 0) nil)
	(t                             t)))
 
;; test for exists function
(exists #'(lambda (x) (eq x 0)) '(-1 0 1))
(exists #'(lambda (x) (eq x 2)) '(-1 0 1))
  

;; all function to check if all the elements of the list 
;; satsfies the condition of the predicate function f
(defun all (f l)
   (not (exists #'(lambda (x) (not (funcall f  x))) l)))

  
(all #'(lambda (x) (> x -3)) '(1 9 6))
(all #'(lambda (x) (> x 2)) '(5 -1 3 3))
	 
;;++++++++++++++++++++++++++++++++++++++++++++++++++++