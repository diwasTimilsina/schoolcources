

<start> = 
I have decided to drop my <major> major because <reason> . Instead, I am going to become a Computer Science major. I have heard that <facts> , and so Computer Science is the best major for me. I am sorry for <apology> , but I believe this is in my best interests. <closing> Take care!
;

<major> = 
  Math
| Economics
| English
| Chemistry
| History
;

<reason> =
  it is not fulfilling
| it is making me feel stupid
| I am too cool
| I have better uses for my time
| I've been bored out of my mind
;

<facts> =
  cool kids take computer science
| computer science majors make enough money to <wealth>
| Duane lets you blow up computers
| CS snacks are the best snacks on campus
;

<wealth> = 
  retire when I'm 25
| buy 503 Google Glasses
| buy 12,409 Big Macs
| buy 90,387 Denny's Grand Slams
| end world hunger
| buyout Microsoft
;

<apology> =
  getting your hopes up
| professing my love for your major prematurely
;

<closing> = 
  I have to go burn my old papers now.
| I'll see around. Or not, since I'll be in lab.
| I have to go install Emacs now. 
;
