/**
 * Hw #8 
 * Tony Liu and Diwas Timilsina   
 */


import java.io.FileInputStream;
import java.util.Scanner;
import scala.util.Random;

// can change to immutable._ if you wish...
import scala.collection.mutable.HashMap; 
import scala.collection.immutable.List;

/*
 * Abstract class the all expandable parts of
 * a grammar extend (Terminal, NonTerminal, Production, Definition).
 */
abstract class GrammarElement {
  
  /**
   * Expand the grammar element as part of a random 
   * derivation.  Use grammar to look up the definitions
   * of any non-terminals encountered during expansion.
   */
  def expand(grammar : Grammar) : String;
  
  /**
   * Return a string representation of this grammar element.
   * This is useful for debugging.  (Even though we inherit a
   * default version of toString() from the Object superclass, 
   * I include it as an abstract method here to ensure that 
   * all subclasses provide their own implementation.)
   */
  def toString() : String;	
  
}


/**
 * Represents a grammar as a map from non-terminal names (Strings) to
 * Defintions.
 */
class Grammar {
  var map : Map[String,Definition] = Map();

  // add a new non-terminal, with the given definition
  def +=(nt : String, defn : Definition) = { map += (nt -> defn); }
  
  // look up a non-terminal, and return the definition, or null
  // if not def exists.
  def apply(nt : String) : Definition = { 
    map.get(nt) match {
      case None => null;
      case Some(v) => v;
    }
  }
  
  // Expand the start symbol for the grammar.
  def expand() : String = { this("<start>").expand(this); }
  
  // return a String representation of this object.
  override def toString() : String = { return map.mkString("\n"); }
}

//stores a list of Productions. Expands by picking a random production to expand.
class Definition extends GrammarElement {
  var l: List[Production] = List[Production]();

  def +=(ge : Production) = { l = l :+ ge;}

  override def expand(grammar : Grammar) : String = { 
    val n = Random.nextInt(l.size);
    l(n).expand(grammar);
  }
  override def toString() : String = { 
    var s : String = "";
    l.foreach(s += _.toString() + " | ");
    s;
  }
}

//stores a list of GrammarElements. Expands each element in its list.
class Production extends GrammarElement {
  var l: List[GrammarElement] = List[GrammarElement](); 

  def +=(ge : GrammarElement) = {l = l :+ ge; }
  override def expand(grammar : Grammar) : String = { 
    var s : String = "";

    l.foreach(s += _.expand(grammar));
    s;
  }
  override def toString() : String = {     
    var s : String = "";
    l.foreach(s += " " + _.toString());
    s;
  }
}

//stores a nonterminal string. Expands by looking up definition of the string
//and recursively expanding.
class NonTerminal(val s: String) extends GrammarElement {
 
  override def expand(grammar : Grammar) : String = { grammar(s).expand(grammar); }
  override def toString() : String = { s; }
}

//strings a terminal string. Expands to itself.
class Terminal(val s: String) extends GrammarElement {
 
  override def expand(grammar : Grammar) : String = { 
    if(s.charAt(0).isLetterOrDigit)
      return " " + s; 
    else return s;
  }
  override def toString() : String = { s; }
}


object RandomSentenceGenerator {
  
    /**
     * Read tokens up to the end of a production and return 
     * them as a Production.
     *
     * Parses "Production ::= [ Word ]*"
     * where word is any terminal/non-terminal.
     */
  protected def readProduction(in : Scanner) : Production = {
    
    val p = new Production();
    
    while (in.hasNext() && !(in.hasNext(";") || in.hasNext("\\|"))) {
      val word = in.next();
      
      // word is next word in production (either a Non-Terminal or Terminal).
      if (word.startsWith("<")) {
	p += new NonTerminal(word);
      }
      else
	p += new Terminal(word);
    }
    p;
  }
  
  /**
   * Read a group of productions and return them as a Definition.
   *
   * Parses "<Definition> ::= <Production> [ '|' <Production> ]* ';'" 
   */
  def readDefinition(in : Scanner) : Definition = {
    
    val d = new Definition();
    
    val production = readProduction(in);
    
    // production is first production for definition
    d += production;
    
    while (in.hasNext("\\|")) {
      expect(in, "\\|");
      val production = readProduction(in);
      
      // production is the next production for definition
      d += production;
    }
    expect(in, ";");
    
    d;  			// return the new production
  }
  
  /**
   * Repeatedly read non-terminal definitions and insert them into
   * the grammar.
   *
   * Parses "<Grammar> ::= [ <Non-Terminal> '=' <Definition> ';' ]*" 
   */
  protected def readGrammar(in : Scanner) : Grammar = {
    
    // the grammar for this generator
    val grammar = new Grammar();
    
    while (in.hasNext()) {
      val name = in.next();
      
      expect(in, "=");
      
      val defn = readDefinition(in);
      
      // defn is next definition to add to grammar
      grammar += (name,defn);
    }
    
    grammar;   // return the grammar
  }
  
  /**
   * A helper method than matches s to the next token returned from
   * the scanner.  If it matches, throw away the token and get ready
   * to read the next one.  If it doesn't match, generate an error.
   * 
   * Since s is used as a regular expression, be sure to escape any
   * special characters like |, which should become \\|
   */
  protected def expect(in : Scanner, s : String) = {
    if (!in.hasNext(s)) {
      println(in.next);
      RandomSentenceGenerator.fail("expected " + s);
    }
    in.next();  // skip s
  }
  
  
  /**
   * Helper method to abort gracefully when an error occurs.
   * <p>
   * Usage: RandomSentenceGenerator.fail("Error Message");
   */
  def fail(msg : String) = {
    throw new RuntimeException(msg);
  }
  
  /**
   * Create a random sentence generator and print out
   * three random productions.
   */
  def main(args: Array[String]) = {

    val grammar = this.readGrammar(new Scanner(scala.io.Source.stdin.mkString));

    println("Grammar is: \n" + grammar);

    for(i <- 0 to 2)
      println(grammar.expand());
  }
}

