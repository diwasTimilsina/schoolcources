# Diwas Matt

import sys
import random
from arff import *

class Node(object):
    """Represents a node in an artificial neural network"""
    def __init__(self):
        # Connections to other nodes
        self.connections = []
        # Weights of those connections
        self.weights = []
        self.value = 0
        self.error = 0

    def getValue(self):
        return self.value

    def add(self, value):
        self.value += value

    def connect(self, other):
        """Creates an output from self into other"""
        self.connections.append(other)
        #self.weights.append( random.uniform(-0.05, 0.05) )
        self.weights.append( .1 )


class Output(Node):
    def __init__(self, className):
        self.className = className
        super(Output, self).__init__()
        
    def __str__(self):
        s = "OUT-{n}({v})".format(n=self.className, v=self.value)
        for i in range(len(self.connections)):
            s += "\n    "
            s += str(self.weights[i])
        return s + "\n"


class Hidden(Node):
    def __init__(self):
        super(Hidden, self).__init__()    

    def __str__(self):
        s = "HIDDEN"
        for i in range(len(self.connections)):
            s += "\n    "
            s += str(self.weights[i])
        return s + "\n"


class Input(Node):
    def __init__(self, name):
        self.name = name
        super(Input, self).__init__()    

    def __str__(self):
        s = "INPUT-{n}({v})".format(n=self.name, v=self.value)
        for i in range(len(self.connections)):
            s += "\n    "
            s += str(self.weights[i])
        return s + "\n"


class ANN(object):
    """Artificial neural network"""
    def __init__(self, attributes, classes, n_hidden=None):
        # One output node for each class
        self.outputs = [Output(c) for c in classes]

        # attributes is list of attrs like [{outlook : [sunny,overcast]}, {humidity : [...]},...]
        self.inputs = []
        for attr in attributes:
            for key in attr:
                self.inputs.append( {value : Input(key + "-" + value) for value in attr[key]} )
        # inputs is list of "clusters":
        # [{"sunny" : Node, "overcast" : Node}, {"hot" : Node, ...}, ...]

        if n_hidden: 
            # If a value has been specified for n_hidden
            self.hiddens = [Hidden() for i in range(n_hidden)]
        else: 
            numInputs = 0
            for inp in self.inputs:
                numInputs += len(inp)
            self.hiddens = [Hidden() for i in range((numInputs + len(self.outputs)) // 2)]

        # Connect hiddens to output, and extra x0 to each hidden
        self.x0h = Input("x0h")
        for h in self.hiddens:
            self.x0h.connect(h)
            for o in self.outputs:
                h.connect(o)

        # Connect extra x0 input node
        self.x0o = Input("x0o")
        for o in self.outputs:
            self.x0o.connect(o)
            
        # Map each input to each hidden node
        for i in self.inputs:
            for value in i:
                # Remember that inputs is list of dicts of attr values => nodes
                node = i[value]
                for h in self.hiddens:
                    node.connect(h)

    def sigmoid(self, y):
        e = 2.71
        return 1 / (1 + e**-y)

    def apply(self, ex):
        """Apply the current ANN weights to this example"""
        # Wipe previous inputs so every input isn't 1 (needed in backprop)
        for cluster in self.inputs:
            for attr in cluster:
                cluster[attr].value = 0

        # Wipe values stored in each node
        for i in self.inputs + [ { None : self.x0h } ]:
            for value in i:
                node = i[value]
                node.value = 0
        for h in self.hiddens + [self.x0o]:
            h.value = 0

        # Apply x0 and w0s to nodes in hidden layer
        for i in range(len(self.x0h.connections)):
            h = self.x0h.connections[i]
            h.add( self.x0h.weights[i] )

        # Apply xi's and wi's to nodes in hidden layer
        for i in range(len(ex)):
            # The input node corresponding to this attr value
            inp = self.inputs[i][ex[i]]
            inp.value = 1
            # Add to each of its connections the weights connecting to inp
            for c in range(len(inp.connections)):
                h = inp.connections[c]
                h.add( inp.weights[c] )

        # Apply the default x0 "hidden layer" node to the outputs
        for i in range(len(self.x0o.connections)):
            o = self.x0o.connections[i]
            o.add( self.x0o.weights[i] )

        # Apply the hidden node values we computed by the weights to the outputs
        for h in self.hiddens:
            h.value = self.sigmoid(h.getValue())
            cxns = h.connections
            for i in range(len(cxns)):
                o = cxns[i]
                o.add( h.getValue() * h.weights[i] )
        for o in self.outputs:
            o.value = self.sigmoid(o.getValue())
                

    def backpropagate(self, training_examples, eta=0.1):
        for ex in training_examples:
            # Strip last part, because that is the class
            self.apply(ex[:-1])
            c = ex[-1]
            # Calculate the error for the output units
            for o in self.outputs:
                target = (0,1)[o.className == c]
                sigma = o.getValue()
                d = sigma * (1 - sigma) * (target - sigma)
                o.error = d

            # Calculate the error for the hidden units
            for h in self.hiddens + [self.x0o]:
                d = h.getValue() * (1 - h.getValue())
                sumterm = 0
                for i in range(len(h.connections)):
                    o = h.connections[i]
                    w = h.weights[i]
                    sumterm += w * o.error
                d *= sumterm
                h.error = d
                # modify the weights from hidden layer to output
                for i in range(len(h.connections)):
                    o = h.connections[i]
                    h.weights[i] += eta * o.error * h.getValue()

            # Modify the weights from input to hidden layer
            for i in self.inputs + [ { None: self.x0h } ]:
                for value in i:
                    node = i[value]
                    for j in range(len(node.connections)):
                        h = node.connections[j]
                        node.weights[j] += eta * h.error * node.getValue()

    def prediction(self, ex):
        self.apply(ex)
        best = -1
        y = None
        for o in self.outputs:
            if o.getValue() > best:
                best = o.getValue()
                y = o.className
        return y


    def __str__(self):
        s = "INPUTS:\n"
        for i in self.inputs:
            for value in i:
                node = i[value]
                s += str(node)
        s += "\nHIDDENS:\n"
        for h in self.hiddens:
            s += str(h)
        s += "\nOUTPUTS:\n"
        for o in self.outputs:
            s += str(o)
        return s
                
if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Error: need .arff")
        exit()
    
    f = open(sys.argv[1])
    lines = f.readlines()

    # attributes of the form [{attr : values},...]
    attributes = get_attributes(lines)

    # [..., {play : [yes, no]} ]
    classes = [ attributes[-1][key] for key in attributes[-1]][0]
    instances = get_instances(lines)

    net = ANN(attributes[:-1], classes)

    correct = 0

    trials = 500
    for i in range(len(instances)):
        #net = ANN(attributes[:-1], classes)
        for t in range(trials):
            train_set = instances[0:i] + instances[i+1:] # Leave one out
            net.backpropagate(train_set)
        test = instances[i]
        y = net.prediction(test[:-1])
        print("predicted " + y)
        print("actual " + test[-1])
        for o in net.outputs:
            print(o.value)
        if y == test[-1]:
            correct += 1
    print("% correct {n}".format(n = correct / len(instances)))

