// Diwas Timilsina 
// Machine Learning, 2014
// to be used with regression. java

// java didn't seem to have pair class so I 
// implemented itself 
public class Pair<L,R> {
    
    private final L left;
    private final R right;

    public Pair(L left, R right) {
	this.left = left;
	this.right = right;
    }

    public L getLeft() { return left; }
    public R getRight() { return right; }
    
    public String toString() {return "["+ left + ","+right+"]";}
    
}
