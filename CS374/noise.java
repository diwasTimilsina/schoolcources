    private Random gen = new Random();

    // Generate a pair of numbers from a normal distribution with
    // the specified mean and variance.
    public Pair noise(double mean, double variance) {
	double u1, u2;
	double v1, v2;
	double x = 0.0;
	double y, xPrime, yPrime;
	double s;
	do {
	    u1 = gen.nextDouble();
	    u2 = gen.nextDouble();
	    v1 = 2 * u1 - 1;
	    v2 = 2 * u2 - 1;
	    s = v1*v1 + v2*v2;
	} while (s >= 1 || s == 0);

	x = Math.sqrt(-2 * Math.log(s) / s) * v1;
	y = Math.sqrt(-2 * Math.log(s) / s) * v2;

	xPrime = mean + Math.sqrt(variance) * x;
	yPrime = mean + Math.sqrt(variance) * y;

	return new Pair(xPrime, yPrime);
    }

