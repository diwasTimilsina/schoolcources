import structure.*;
import java.util.Random; 
import java.util.Scanner; 



/*
 * Diwas Timilsina
 * lab 1 
 * Feb 10 2013 
 */
 

/*     Thought Questions
 *  1. it's can be done by thinking it as fliping a coin. if it's a head, there
 *     are three coins. if it's a tail, filp again. if you get head this time 
       it would be  the fourth coin, if it's tail flip again.. and so on.

 *  2. to prevent a game from being immediately won, one can leave first
 *     n-spaces empty (n would be the number of coins) and then start filling
 *     coins on the board from that point. 
 
 *  3. computer could provide hint by giving players the coin number 
 *     and the  number of steps that the coin can be moved to win the game. 
 
 *  4. we can write a method computerPlay that would select a coin and 
 *     look for the legal move and make the move.

 *  5. I have taken this possiblity into consideration. While filling the 
 *     coins on the board, the program would generate random location on the
 *     until there are no coins at that location     
 */


  /* This class is designed to create a simple silver dollar game played 
   * between two players. The board on which the game is played is represendted 
   * by one dimensional array. The goal of this game is to move all the coins 
   * to the leftmost squares on the board. But remember the coins cannot overlap
   * The game is won when all the coins are at the first three squares of the
   * board. As each moves are very precious in the game, when a player makes a
   * illegal move, the computer automatically switches the turn to the next 
   * player
   */

   public  class CoinStrip {
        
       // the board on which the game is played
       private int [] board;  
    

       // main method of the class                         
    public static void main (String args[]){
	
	Scanner in = new Scanner (System.in);

        // allows players to choose the length of the board
        System.out.println ("Enter the length of the game board:"); 
        int length = in.nextInt();
        

        // allows players to input the number of coins in the game
        System.out.println (" How many coins do you want in this game?");
        int numCoins =in.nextInt();
        
          // to prevent verylong and invalid board length input and num of 
          // coins  
        if (length > 20 || length < 0 || numCoins > length){
	    length = 20;
        }
        
        // constructs new CoinStrip and draws the board
        CoinStrip c = new CoinStrip(length, numCoins);
	c.updatePrint();
            
        int n = 0 ;
        int n1 = 0 ;
      
	// while the game is not over, take the inputs from the players and
        // move the desired coins. if the players provide invalid input, swith 
	// to other player's turn
	while (! c.gameOver(numCoins)){
      try{
	  System.out.println ("Choose your coin, Player 1:"); 
	  n =in.nextInt();
	  System.out.println ("How many Steps do you want to move?");
	  n1 = in.nextInt();
      } catch ( Exception e) {
          System.out.println ("Invalid Input, Next Player's turn");
	  String s =  in.next();}      
               c.move (n,n1); 
        
	try {
           System.out.println ("Choose your coin, Player 2:"); 
           n =in.nextInt();
           System.out.println ("How many Steps do you want to move?");
           n1 = in.nextInt();
	} catch ( Exception e) {
	    System.out.println ("Invalid Input, Next Player's turn");
            String  s = in.next() ; } 
         	c.move(n,n1);
        }
    }
     
       // constructor for the class
       public CoinStrip(int boardLength, int numCoins) { 
       board = new int [boardLength];
          Random r = new Random();
          int random =r.nextInt(boardLength);
       
	  // fills the entire board with zeros
          for (int i = 0; i < board.length;i++){
	    board [i] = 0;
          }
      
	  // fills the board with required number of coins
	  for ( int i = 1; i<= numCoins; i ++){   
	    
             while ( board[random] != 0){
		random = r.nextInt(boardLength);  
         }  
	    board[random]=i;   
        } 
      }
     
      
       // update the print on the board
      private void updatePrint(){
      
	String s = ""; 
       
        for ( int i = 0; i< board.length; i++){
	    s = s +"|"+ board[i];
        }
         
    	System.out.println("+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_"); 
        System.out.println(s);
        System.out.println("+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_");       
       }


       // checks wheather the first three elements in the array are numbers 
       // other than zero. if it is the case the game is over and the method
       // will return true. Otherwise, game is not won and the method 
       // will return false.    
        
        private boolean gameOver(int numCoins){
	  for( int i = 1; i < numCoins; i++){
	     if (board[i] == 0){ 
                 return false; 
              } 
          } 
          System.out.println(" Game Over, Thanks for Playing!");
           return true;  
         }

     
       // the method takes the coin to be moved and the number of steps to be 
       // moved as parameters and moves the given coin by given steps on the
       // board

       private void move (int coin, int steps){
	   int index = getIndexOf(coin);
           if (index != -1 && steps > 0){
	       if ((index-steps)>=0 && board[index-steps]==0){
		   board[index-steps]= coin;
                   board[index]=0;
               }else{
                System.out.println("Invalid Input, Next Player's turn");
               }
           }else{
	       System.out.println("Invalid Input, Next Player's turn");
           }
           updatePrint();
       }


       // method that returns the index of the given coin
     private int getIndexOf( int coin){
	 for (int i=0; i< board.length; i++){
             if (board[i]==coin){
		 return i;
             }
         } 
         return -1; 
     }  
    
 }   