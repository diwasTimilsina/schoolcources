
import structure5.*;
import java.util.Random;
import java.util.Iterator;
import java.util.Scanner;

/**
 * This class implement human player.When playing as a human, we check to see if the game has already
 * been won by the opponent. if that is the case, the winner is the opponent player. Otherwise, we print   
 * the board, present the possible moves, allow user to input the moves and had off the play to an opponent
 *
 * Diwas Timilsina
 * 5/9/2013
 */

public class HumanPlayer implements Player{
    // char that represents player
    private char player;
    //scanner to take input
    private Scanner scan;

    // constructor that initializes the player and scanner
    public HumanPlayer(char color){
	Assert.pre(color == HexBoard.WHITE || color == HexBoard.BLACK,"Invalid Player");
	player = color;
	scan = new Scanner(System.in);
    }
    
    // plays the game by printing the current board and check to see if 
    // the opponent hasn't already won the game. if the opponent has already won the game,
    // return opponent as a winner. Otherwise, present the possible moves to the user and 
    // ask to select a move. Finally had the play to the oppoenent
    public Player play (GameTree node, Player opponent){
	HexBoard board = node.board();
	node.printBoard();
	if(node.childSize() == 0){
	    return opponent;
	}else if(board.win(HexBoard.opponent(player))){
	    return opponent;
	}else {  
	    node.printMoves();   
	    System.out.println("'"+ this +"'"+ " Select your Move: ");
	    int nextMove = scan.nextInt(); 
	    Assert.pre(nextMove < node.childSize(),"Bad Move Selection"); 
	    node = node.getChild(nextMove);
	    return opponent.play(node,this);
	}
    } 

    // doesn't have any use when player is human. It is only used when the 
    // player is a computer
    public Player notPrintPlay(GameTree node, Player opponent){
	return null;
    }
    
    // to print the player's character representation
    public String toString(){
	return ""+ player;
    }   

    //code to test the program..
    public static void main (String args [] ){
	Player player1 = new HumanPlayer(HexBoard.WHITE);
	Player player2 = new HumanPlayer(HexBoard.BLACK);
	HexBoard board = new HexBoard ();
	char player = HexBoard.WHITE;
	GameTree game = new GameTree(board, player);
	Player winner = player1.play(game,player2);
	System.out.println("The Winner is: "+ winner.toString());
    }
}

