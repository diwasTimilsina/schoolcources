
import structure5.*;

/**
 * Class that build a game tree using hexboards.
 *
 * Diwas Timilsina
 * 5/9/2013
 */

public class GameTree {
   
    // vector to keep track of childrens
    private Vector<GameTree> pointer;
    
    //color of the player
    private char color;
    
    // current board
    private HexBoard root; 
    
    //total size of tree
    public static int size; 

    // the constructor for the GameTree that takes a board and a color for which
    // the game tree is being built. 
    public GameTree(HexBoard board, char aColor){
	Assert.pre(aColor == HexBoard.WHITE || aColor == HexBoard.BLACK,"Invalid Player");
	
	// increment the size of the tree
	size ++;
      
	// if the current board is not a win, add all the possible moves from that location to the tree 
	pointer = new Vector<GameTree>();
	root = board;
	color = aColor;
	if (!root.win(HexBoard.opponent(color))){
	    Vector<HexMove> moves = root.moves(color);
	    for(int i = 0; i < moves.size() ; i++){
		HexMove move = moves.get(i);
		HexBoard newChild =new HexBoard(root,move);
		GameTree child = new GameTree(newChild,HexBoard.opponent(color));
		pointer.add(child);
	    }
	}
    }
    
    //total number of possible moves in the game tree 
    public static int treeSize(){
	return size;
    }
    
    // return the character representing player
    public char returnPlayer(){
	return color;
    }

    //return the board
    public HexBoard board(){
	return root;
    }
    
    // no of total possible moves from a position
    public int childSize(){
	return pointer.size();	
    }
    
    // remove the move at index i
    public void removeMove(int i){
	pointer.remove(i);
    }
    
    //print the current board
    public void printBoard(){
	System.out.println(root);
    }
    
    // get the indicated node  
    public GameTree getChild(int i){
	return pointer.get(i);
    }
    
    //print the available moves from the current location
    public void printMoves(){
	Vector<HexMove> move = root.moves(color);
	for(int i = 0; i < move.size(); i++){
	    System.out.println((i)+ " " + move.get(i));
	}
    }
    
    // method to test the code
    public static void main (String args []){
	HexBoard b = new HexBoard();
	char color = HexBoard.WHITE;
	GameTree game = new GameTree(b,color);
	int count = game.treeSize();
	System.out.println(count);
    }
}