import structure5.*;
import java.util.Scanner;
import java.util.Random; 

// This class is responsible for creating a vector of assoctaiton in 
// which string would be the key and frequency list its value

    public class Table{
   
         // association of the string and frequencyList
         protected Vector <Association<String,FrequencyList>> mapTable; 
       

          // the constructor of the class
          public Table (){
    
	     mapTable = new Vector <Association<String,FrequencyList>>();   
        
     }
   
       
     // post: takes a string of length k+1 and finds frequency list for 
     //        str.substring(0,k) in the table, or creates new empty frequency
     //        list for substring and adds it to the table; then it adds k+1th
     //        character of str to FrequencyList  
     public void add (String str){
        int end = str.length()-1;
	String required = str.substring(0,end);
        String s = str.substring(end,str.length());
     
	for( int i= 0; i < mapTable.size(); i++){
	    Association<String, FrequencyList> match =mapTable.get(i);
            if ( match.getKey().equals(required)) {
		match.getValue().add(s);
                return;
             }                      
	}
	 
	FrequencyList freq = new FrequencyList(); 
	freq.add(s); 
	Association <String, FrequencyList> assoc = 
	    new Association <String,FrequencyList> (required,freq);
	mapTable.add(assoc);  
    } 


    // to print table for debugging 
    public String toString(){
        String answer = "";
     
	for( int i=0; i < mapTable.size(); i++){
	    Association <String,FrequencyList> temp = mapTable.get(i);
            answer = answer +"After "+ temp.getKey() +"......."+
	    temp.getValue().toString() + "\n";   
        }
        return answer; 
    }


      
     //  takes a string and lookes through the table. if the string is found, 
     //  the method gets the value of its association. using this value calls 
     //  the pickNext() method of the frequencylist class 
     public String pickNext(String str){
         Association <String,FrequencyList> temp;
         for ( int i = 0; i < mapTable.size(); i++){
	     temp = mapTable.get(i);
             if (temp.getKey().equals(str)){
	         return temp.getValue().pickNext();                        
	      }   
          } 
	 // to deal with the problem of not finding words in the frequency list
	      return "e";
	}
      
        
      // static main method for debugging 
     public static void main (String args []){
           Table t = new Table();
	String s = "the theater is their thing";
        int start = 0;
        int end = 3;
	while (end < s.length()){
	    t.add(s.substring(start,end)); 
	    start++;
	    end++; 
         }
	   System.out.println(t); 
      }

  }
