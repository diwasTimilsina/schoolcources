import structure5.*;
import java.util.Iterator;

/*
 *  Diwas Timilsina
 *
 * This class iterates through a vector creating subsets.
 *
 */

public class SubsetIterator<E> extends AbstractIterator<Vector<E>> {
    // The vector
    protected Vector<E> data; 
    // A count
    protected long counter; 

    // The constructor takes a vector as a parameter
    public SubsetIterator (Vector<E> aData){
	// The data instance variable vector is set to the one passed in
	data = aData; 
	
	// Resets
	reset(); 
    }

    // Sets the count to 0
    public void reset(){
	counter = 0;
    }

    // Tests if there are anymore subsets
    public boolean hasNext(){
	// There are 2^n - 1 subsets. The size of the data vector is n.
	return counter <= (Math.pow(2,data.size()) - 1); 
    }
    
    // Gets the next subset
    public Vector <E> get(){
	// creates a new vector
	Vector <E> vect = new Vector<E>(); 

	// Uses binary numbers to determine if an element from the original
	// vector should be included
	for (int i = 0; i < data.size() ; i++){ 
	    // If the counter & 1 with the 1 shifted to the current element
	    // is not 0, then that element from the original vector is added
	    // to the subset vector
	    if((counter & (1L << i)) > 0){
		vect.add(data.get(i)); 
	    } 
	}
	return vect; 	
    }  
    
    // Gets the next subset
    public Vector<E> next(){
	// Uses the get method to get a subset vector
	Vector <E> temp = get();

	// Increments count
	counter++;

	// Returns the subset
	return temp;
    }

    // A main method that tests our code.  Creates a vector of integers 1 
    // through 8. Then iterates through it and prints each subset. Counts 
    // the number of subsets
    public static void main (String[] args){
	Vector<Integer> test = new Vector <Integer>();
	for(int i = 0; i < 8; i++){
	    test.add(i);
	}
	
	SubsetIterator <Integer> iter = new SubsetIterator <Integer>(test);
	int count = 0;
	while (iter.hasNext()){
	    System.out.println(iter.next()); 
	    count++;
	}
	
	System.out.println("total " + count); 
    }
}   
