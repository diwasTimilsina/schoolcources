
import structure5.*;
import java.util.*;

/**
-------------------------------
Diwas Timilsina
CS 136 lab
29 April 2013
-----------------------------
 * This class represents one creature on the board.
 * Each creature must remember its species, position, direction,
 * and the world in which it is living.
 * <p>
 * In addition, the Creature must remember the next instruction
 * out of its program to execute.
 * <p>
 * The creature is also repsonsible for making itself appear in
 * the WorldMap.  In fact, you should only update the WorldMap from
 * inside the Creature class.  
 */

public class Creature {
    /**
     * Create a creature of the given species, with the indicated
     * position and direction.  Note that we also pass in the 
     * world-- remember this world, so that you can check what
     * is in front of the creature and to update the board
     * when the creature moves.
     */
    
    // species of the creature
   private Species spec; 
    //the world where the creature lives
   private World<Creature> world;
    //the position of the creature in the world
   private Position pos; 
    //the direction on which the creature is facing
   private int dir;
    // random position generator
   private Random ran;
    //count for the instruction
   private int count;
    
    /**
     * Create a creature with a species, world, initial position and direction
     */
    
    public Creature(Species species,
		    World<Creature> aWorld,
		    Position aPos,
		    int aDir) {
	//save the variables as local variables
	world = aWorld;
	spec = species;
	pos = aPos;
	dir = aDir;
	ran = new Random();
	count = 1;
    }

    /**
     * Return the species of the creature.
     */
    public Species species() {
	return spec;
    }

    /**
     * Return the current direction of the creature.
     */
    public int direction() {
	return dir;
    }

    /**
     * Return the position of the creature.
     */
    public Position position() {
	return pos;
    }

    /**
     * Execute steps from the Creature's program until 
     * a hop, left, right, or infect instruction is executed.
     */
    public void takeOneTurn() {
	Instruction inst;
	int step; 
	//keep on reading instructions until one of the instructions is hop or left or right or infect
	while(true){
	    inst = spec.programStep(count);
	    step = inst.getOpcode();
	   
	    // if it is a hop, check if the adjacent position is within the world and is empty
	    // if the condition is satisfied then move the creature by one step, update the print
	    // and increment the count and break out of the while loop. if the condition is not met, 
	    // increment the count and break out of the while loop. 
	    if (step == Instruction.HOP){
		if (world.inRange(pos.getAdjacent(dir)) && world.isEmpty(pos.getAdjacent(dir))){
		    updatePrint(pos," ", dir, " ");
		    world.set(pos,null);
		    pos = pos.getAdjacent(dir);
		     world.set(pos,this);
		     updatePrint(pos,spec.getName(),dir,spec.getColor());
		     count++;
		    return;
		}else{
		    count ++;
		    return;
		}

	    //if the instruction is left, then turn left, increment count and break out of the loop	
	    }else if (step == Instruction.LEFT){
		dir = leftFrom(dir);
		updatePrint(pos,spec.getName(),dir,spec.getColor());
		count ++;
		return;

	    // if the instruction is a right, then turn right, increment count and break out of the loop
	    }else if (step == Instruction.RIGHT){
		dir= rightFrom(dir);
		updatePrint(pos,spec.getName(),dir,spec.getColor());
		count ++;
		return;

	     //if the instruction is a go, just go to the address go refers to
	    }else if (step == Instruction.GO){
		count = inst.getAddress();

	     //if the instruction is an infect, check to see if the adjacent position is within the 
	     //world, is not empty, and contains a creature of different species
	     //if the condition is met, change the species of the creature to that of yours, update print, 
	     //increment count and break out of the loop
	     // if the condition is not met, increment count and break out of the loop	
	    }else if (step == Instruction.INFECT){
		if (world.inRange(pos.getAdjacent(dir)) && !world.isEmpty(pos.getAdjacent(dir))
		    && ((Creature)world.get(pos.getAdjacent(dir))).species() != spec){
		    Creature enemy = (Creature)world.get(pos.getAdjacent(dir));
		    enemy.spec=spec;
		    enemy.count = inst.getAddress();
		    updatePrint(pos.getAdjacent(dir),enemy.species().getName(),dir,enemy.species().getColor());
		    count++;
		    return;
		} else{
		    count++;
		    return;
		}

	    // if the instruction is if emeny, check to see if the adjacent position is in the world and 
	    // it is empty. if the condition is met, go to the address that if enemy refers. otherwise
	    // just increment count
	    } else if (step == Instruction.IFEMPTY){
		if (world.inRange(pos.getAdjacent(dir)) && world.isEmpty(pos.getAdjacent(dir))){
		    count  = inst.getAddress();
		} else{
		    count++;
		}
		
	     // if the instruction is a wall, check if the adjacent position is not in the world. if the
	     // condition is met, go to the address refered. otherwise just increment count
	    } else if (step == Instruction.IFWALL){
		if (!world.inRange(pos.getAdjacent(dir))){
		    count = inst.getAddress();
		} else{
		    count++;
		} 
	
	    //if the instruction is ifenemy, check to see if adjacent position is in the world, is not empty
	    // and contains creature of different species. if the conditions are met, go to the refered
            // address. otherwise, increment count		
	    } else if (step == Instruction.IFENEMY){
		if (world.inRange(pos.getAdjacent(dir)) && !world.isEmpty(pos.getAdjacent(dir)) && 
		    ((Creature)world.get(pos.getAdjacent(dir))).species() != spec){
		    count = inst.getAddress();
		} else {
		    count ++;
		}
	    
	     //	if the instruction is ifrandom, half the time go to the refered address, half the time go 
	     // to just increment count	
	    } else if (step == Instruction.IFRANDOM){
		int n = ran.nextInt(2);
		if (n == 0){
		    count = inst.getAddress();
		}else{
		    count++;
		}    
	    //if the instruction is ifsame, check to see if the adjacent position is in the world, is not 
	    //empty and contains a creature of same species. if the condition is met, go to the refered 
	    // address. Otherwise, just increase the counter	
		
	    } else if (step == Instruction.IFSAME){
		if (world.inRange(pos.getAdjacent(dir)) && !world.isEmpty(pos.getAdjacent(dir)) && 
		    ((Creature)world.get(pos.getAdjacent(dir))).species() == spec){
		    count = inst.getAddress();
		}else{
		    count++;
		}

	    // if non of the instructions above is found, then the instruction is a bad instruction	
	    }else{
		Assert.fail("bad instruction");
	    }      
	}	    
      
    }
    // updates the display
    private void updatePrint(Position position, String name, int direct, String color ){
	WorldMap.displaySquare(position,name.charAt(0),direct,color);
    }

    /**
     * Return the compass direction the is 90 degrees left of
     * the one passed in.
     */
    public static int leftFrom(int direction) {
	Assert.pre(0 <= direction && direction < 4, "Bad direction");
	return (4 + direction - 1) % 4;
    }

    /**
     * Return the compass direction the is 90 degrees right of
     * the one passed in.
     */
    public static int rightFrom(int direction) {
	Assert.pre(0 <= direction && direction < 4, "Bad direction");
	return (direction + 1) % 4;
    }

    /**
     * Code to test the Creature class 
     */
    public static void main(String st[]) {
	World wo = new World(15,15);
	Species sp = new Species("Hop.txt");
	Position po = new Position (5,5);
	Creature cre = new Creature (sp,wo,po,1);
	
	System.out.println(cre.direction());
	
	for(int i = 0; i < 1; i++){
	    cre.takeOneTurn();	
	    System.out.println(cre.direction());
	}
    }
}
