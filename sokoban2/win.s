# A translation of <Duane Bailey's> version of win().
# (c) 2013 < Diwas Timilsina>

# first, allocate room in the "data" area of the program
# for a globally accessable array, BDay.  It consists of 3 longs.
	.data
	.align	4  # start on a long-word boundary
	.globl	BDay
# .type and .size gdb a hint as to the meaning of "BDay"
	.type	BDay,@object
BDay:	
	.long	12,15,1960	# your birthday
	.size	BDay,.-BDay

# these are constants we might use in the code;
# ripped shamelessly from sokoban.h
	.equ	BOX,2
	.equ	STORE,4
	
# standard prolog for functions we write:
	.text
	.globl	win
	.type	win,@function
# Level pointer l (quad) appears in rdi, put it in rbx
# r = r12d
# c = r13d
# h = r14d
# w = r15d

	
win:
	# first, save the regsters rbx, r12-r15 
	pushq 	%rbx
 	pushq	%r12
	pushq 	%r13
	pushq	%r14
	pushq   %r15

	# move parameters into local temporaries
	# (necessary because we're calling other routines)
	movq	%rdi,%rbx	# save level pointer l
	
	# for h
	movq	%rbx, %rdi	# first parameter l
	call 	height		# height(l)
	movl	%eax,%r14d	# save value in h

	# for w
	movq	%rbx,%rdi	# first parameter l
	call	width		# width(l)
	movl	%eax,%r15d	# save value in w

	# first outer loop starts
	movl	$0, %r12d	# r = 0
oloop:	cmpl	%r14d, %r12d	# r ?? h 
	jge 	endoloop	# r > = h - leave loop

	# first inner loop starts
	movl 	$0, %r13d	# c = 0
iloop:	cmpl	%r15d, %r13d	# c ?? w
	jge	endiloop	# c > = w -leave loop

	# compute d
	movq 	%rbx, %rdi	# first parameter l
	movl	%r12d, %esi	# second parameter r
	movl	%r13d, %edx	# third parameter c
	call 	get		# get(l,r,c)
	movl 	%eax, %ecx	# save the value in d

	# if part
	movl	%ecx,%r8d	# saves a copy of d in a temporary variable
	andl 	$BOX, %ecx	# d & BOX
	jz	overif		# if (d & BOX) equals 0 - leave the if statement 
	andl	$STORE, %r8d	#  d & STORE
	jnz	overif		# if (d & STORE) not equals to 0 - leave the if statement
	movq 	$0, %rax	# result = 0
	jmp	fini		# jump to fini if result equals 0
overif:
	incl	%r13d		# c ++
	jmp 	iloop		# jump to iloop
endiloop:
	incl	%r12d		# r ++
	jmp 	oloop		# jump to oloop
endoloop:

	# second outerloop
	movl	$0, %r12d	# r = 0
soloop:	cmpl	%r14d, %r12d	# r ?? h
	jge	endsoloop	# r > = h - leave loop

	# second inner loop
	movl 	$0, %r13d	# c = 0
siloop: cmpl 	%r15d, %r13d	# c ?? w
	jge 	endsiloop	# c > = w - leave loop


	# compute d
	movq	%rbx, %rdi	# first parameter : l
	movl 	%r12d, %esi	# second parameter: r
	movl 	%r13d, %edx	# third parameter: c
	call    get 		# get (l,r,c)
	movl    %eax, %ecx	# save the result in d

	# if part
	andl 	$BOX, %ecx	# BOX & d
	jz	elseif		# if (Box & d) = 0 - goto else part
	
	# highlight
	movq	%rbx, %rdi	# first parameter: l
	movl	%r12d, %esi	# second parameter: r
	movl	%r13d, %edx	# third parameter: c
	call	highlight	# highlight(l,r,c)
elseif:
	incl 	%r13d		# c++
	jmp	siloop		# jump to second inner loop
endsiloop:
	incl	%r12d		# r ++
	jmp 	soloop		# jump to second outer loop
endsoloop:
	movq 	$1, %rax	# result = 1
	
fini:
	popq 	%r15
	popq	%r14
	popq	%r13
	popq	%r12
	popq	%rbx
	ret
	
# this computes the size of win.  "." is the current address
# this is a help to the debugger (should you need it)
	.size	win,.-win
