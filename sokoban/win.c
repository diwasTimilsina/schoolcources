// A routine for detecting a win.
// (c) 2013 <Diwas Timilsina>
#include "sokoban.h"

// Birthday for advanced biometric tracking:
int BDay[] = { /* this is Duane's: */ 11, 8, 1992};

// Check for a win (called from play, in sokoban.c).
// Return 1 iff all the BOX locations are also STORE locations.
// Duane: Note how I'm not refering directly to any part of the opaque type l!
int win(level *l)
{
  // height of the array
  int arrayHeight =  height(l);

  // width of the array
  int arrayWidth =  width(l);

  //keep track of height and width 
  int i;
  int j;

  //current char
  char currentPos;
  
  // if the character at the current position
  // its just a box(not box and store) then
  // the game isn't won yet
  for(i=0; i< arrayHeight; i++){
    for(j=0; j < arrayWidth; j++){
      currentPos = get(l,i,j);
      if (BOX == (currentPos & (STORE|BOX))){
	return 0;
      }
    }
  }
  

  // goes through the array one more time and 
  // highlights the place where there is a box and
  // store. At the end, return 1 to indicate the game
  // is won
  for(i = 0; i < arrayHeight ; i++){
    for (j = 0; j < arrayWidth ; j++){
      currentPos = get(l,i,j);
      if ((STORE|BOX)== (currentPos &(STORE|BOX))){
	 highlight(l, i, j);
      }
    }   
  }  
  return 1;
}
